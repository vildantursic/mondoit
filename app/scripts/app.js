var app = angular.module('app', ['pascalprecht.translate', 'ui.router', 'ngSanitize', 'ngCookies']);

app.run(['$rootScope', function ($rootScope) {
  $rootScope.$on('$viewContentLoaded',function(){
    jQuery('html, body').animate({ scrollTop: 0 }, 200);
  });
}]);
