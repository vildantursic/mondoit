var app = angular.module('app');

app.controller('cookiesCtrl', ['$scope', '$translate', '$cookies', function($scope, $translate, $cookies){
   
    var cookiesApproved = $cookies.get("cookies-approved");
    var cookie = document.getElementById("cookie");

    $scope.hideCookies = function () {
        cookie.className += " hide";
        $cookies.put("cookies-approved", true);
    };

    if (cookiesApproved){
        $scope.hideCookies()
    }
}]);
