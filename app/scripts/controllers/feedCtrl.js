var app = angular.module('app');

app.controller('feedCtrl', ['$scope', '$translate', 'FeedService', function($scope, $translate, Feed){

  $scope.loadButonText="Load";
  $scope.loadFeed=function(){
      var r = Math.floor((Math.random() * 3));
      Feed.parseFeed($scope.feedSrc[r]).then(function(res){
          $scope.feeds=res.data.responseData.feed.entries;
      });
  };
  $scope.feedSrc= ["https://www.onlinehaendler-news.de/payment?format=feed&type=rss", "https://www.onlinehaendler-news.de/marketing?format=feed&type=rss", "https://www.onlinehaendler-news.de/e-commerce-tipp?format=feed&type=rss"]
  $scope.loadFeed();
}]);

app.factory('FeedService',['$http',function($http){
    return {
        parseFeed : function(url){
            return $http.jsonp('//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=50&callback=JSON_CALLBACK&q=' + encodeURIComponent(url));
        }
    }
}]);
