var app = angular.module('app');

app.controller('headerCtrl', ['$scope', '$translate', '$cookies', function($scope, $translate, $cookies){

  // MENU

  var i = 0;
  var menuList = document.getElementsByClassName("menu-selector");
  var menu = document.getElementById("menu");

  $scope.menu = function(s) {

    setTimeout(function () {
      if(i == 0 && s == 0){
        menu.className += " show-menu";
        i = 1;
      }
      else if (i == 1 && s == 0){
        menu.className = "menu";
        i = 0;
      }
      else if (i == 1 && s == 1){
        menu.className = "menu";
        i = 0;
      }
    },100);

  };

  // LANGUAGE

  $scope.languages = [
    {
      id: 0,
      lng: "en"
    },
    {
      id: 1,
      lng: "de"
    }
  ];

  $scope.activeLang = function (lng) {
    $("#en-m").removeClass("active-lang");
    $("#de-m").removeClass("active-lang");

    if (lng == "en"){
      $("#en-m").addClass("active-lang");
    } else if (lng == "de") {
      $("#de-m").addClass("active-lang");
    }
  };

  var lang = $cookies.get("lang");

  if (lang == undefined) {
    $scope.selectedLang = 'de';
    $scope.activeLang($scope.selectedLang);
  } else {
    $scope.selectedLang = lang;
    $translate.use($scope.selectedLang);
    $scope.activeLang($scope.selectedLang);
  }

  $scope.changeLanguage = function(id) {
    $scope.activeLang($scope.languages[id].lng);

    $translate.use($scope.languages[id].lng);
    $cookies.put("lang", $scope.languages[id].lng);
  };

}]);
