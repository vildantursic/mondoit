var app = angular.module('app');

app.controller('projectsCtrl', ['$scope', '$timeout', function($scope, $timeout){

  $scope.references = [
    {
      id: 0,
      name: "NUTRICODIET",
      img: "0",
      credit: "MondoIT",
      client: "MondoIT",
      category: "Web app",
      description: "php, mySQL"
    },
    {
      id: 1,
      name: "METZGEREI ARNOLD",
      img: "1",
      credit: "Vildan Tursic",
      client: "Metzgerei Arnold",
      category: "Web app",
      description: "php, mySQL"
    },
    {
      id: 2,
      name: "MY STREET TALK",
      img: "2",
      credit: "Zoran Kocevski",
      client: "My Street Talk",
      category: "Web App",
      description: "php, mySQL"
    },
    {
      id: 3,
      name: "LUXTOGO",
      img: "3",
      credit: "MondoIT",
      client: "Tamara Nikolic",
      category: "Web App",
      description: ""
    },
    {
      id: 4,
      name: "HEART & RHYTHM SOLUTIONS",
      img: "4",
      credit: "MondoIT",
      client: "Heart&Rhythm Solution",
      category: "Web App",
      description: "php, mySQL"
    },
    {
      id: 5,
      name: "WEE TALK",
      img: "5",
      credit: "Vladimir Pavlovic",
      client: "Wee Talk",
      category: "Mobile App",
      description: "iOS"
    },
    {
      id: 6,
      name: "KUNDE",
      img: "6",
      credit: "Vladimir Pavlovic",
      client: "Octilla",
      category: "Mobile App",
      description: ""
    },
    {
      id: 7,
      name: "KUNDE",
      img: "7",
      credit: "Igor Gavric",
      client: "Mobile Business",
      category: "Web App",
      description: "php"
    },
    {
      id: 8,
      name: "KUNDE",
      img: "8",
      credit: "",
      client: "",
      category: "",
      description: ""
    },
    {
      id: 9,
      name: "KUNDE",
      img: "9",
      credit: "",
      client: "",
      category: "",
      description: ""
    },
    {
      id: 10,
      name: "KUNDE",
      img: "10",
      credit: "",
      client: "",
      category: "",
      description: ""
    },
    {
      id: 11,
      name: "KUNDE",
      img: "11",
      credit: "",
      client: "",
      category: "",
      description: ""
    },
    {
      id: 12,
      name: "KUNDE",
      img: "12",
      credit: "",
      client: "",
      category: "",
      description: ""
    },
    {
      id: 13,
      name: "KUNDE",
      img: "13",
      credit: "",
      client: "",
      category: "",
      description: ""
    },
    {
      id: 14,
      name: "KUNDE",
      img: "14",
      credit: "",
      client: "",
      category: "",
      description: ""
    },
    {
      id: 15,
      name: "KUNDE",
      img: "15",
      credit: "",
      client: "",
      category: "",
      description: ""
    },
    {
      id: 16,
      name: "KUNDE",
      img: "17",
      credit: "",
      client: "",
      category: "",
      description: ""
    },
    {
      id: 17,
      name: "KUNDE",
      img: "18",
      credit: "",
      client: "",
      category: "",
      description: ""
    },
    {
      id: 18,
      name: "KUNDE",
      img: "19",
      credit: "",
      client: "",
      category: "",
      description: ""
    },
    {
      id: 19,
      name: "KUNDE",
      img: "20",
      credit: "",
      client: "",
      category: "",
      description: ""
    },
    {
      id: 20,
      name: "SHOPS",
      img: "21",
      credit: "",
      client: "",
      category: "",
      description: ""
    }

  ]

  var buttons = [];

  $timeout(function(){
    buttons = document.querySelectorAll("div.r");
  }, 2000);

  var last = 0;
  $scope.manipulate = function(data){

    buttons[data].className = "references-box more-about-reference";

    if (last != data){
      buttons[last].className = "references-box";
    }

    last = data;
  };

}]);
