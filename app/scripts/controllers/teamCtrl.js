var app = angular.module('app');

app.controller('teamCtrl', ['$scope', function($scope){

  $scope.team = [
    {
      id: 0,
      name: "Kosmas",
      title: "Sales Manager",
      img: "kosmas",
      phone: "0152/26063684",
      email: "k.fischer@mondoit.de",
      skills: [],
      about: "Damit wir mit Ihnen erfolgreich sein können, benötigt es den engen Kontakt zu Ihrem Unternehmen. <br> Mit Kosmas finden sie zu Ihrem Fachbereich eine perfekte Verbindung zu Unseren Informatikern. <br> Sein künstlerisches Denken, sowie seine Fachkenntnisse im Kundenservice und Dienstleistung schafft eine sehr anregende Atmosphäre der Zusammenarbeiten. <br> Werden Sie mit Ihm zum dynamischen Duo. <a href='mailto:k.fischer@mondoit.de?subject=MondoIT'>k.fischer@mondoit.de</a> <br> Tel: 015226063684"
    },
    {
      id: 1,
      name: "Lejla",
      title: "Designer",
      img: "lejla_imamovic",
      phone: "000 111 222 333",
      email: "lejla.imamovic@mondoit.de",
      skills: ["Photoshop", "CS", "Ilustrator"],
      about: "Lejla hat einen Abschluss in Visual Communication Design. Die Grafik Designerin und Interface Designerin war bereits für verschiedene Projekte verantwortlich: Erstellen von komplexen Konzepten, Grafiken, Layouts und Corporate Identity Richtlinien für Marketing Materialien jeglicher Art. Darüber hinaus hat Lejla Erfahrung im 3D Modeling und Digital Effects."
    },
    {
      id: 2,
      name: "Nikolina",
      title: "Senior UX & UI Designer",
      img: "nikolina",
      phone: "000 111 222 333",
      email: "nikolina.jezidzic@mondoit.de",
      skills: ["UI & UX DESIGN", "DIGITAL MARKETING", "ATRAKDIFF", "ADOBE PHOTOSHOP", "GOOGLE ANALYTICS"],
      about: "Nikolina is Senior UX designer with great passion for graphic design and packaging as well. She has over 5 years of hands-on work experience in UI/UX design, rapid prototyping, and front-end development for rich Internet applications and mobile devices. Her responsibility is creating simple and delightful user experiences utilizing principals of user-centered design, to deliver intuitive design solutions."
    },
    {
      id: 3,
      name: "Bettina",
      title: "Social Media Manager",
      img: "bettina",
      phone: "000 111 222 333",
      email: "bettina@mondoit.de",
      skills: [],
      about: "Social Media hat heute seinen festen Platz im Marketing-Mix. Und jedes Unternehmen, das soziale Medien einsetzt steht vor der Herausforderung,  (fast) täglich frischen Content zu erstellen und die Gemeinschaft der Fans zu pflegen. Community-Managerin Dipl.-Oec. Bettina Halbach erstellt  konsequent  einzigartige und aktuelle Inhalte für Social Media Kanäle wie Facebook, Instagram, Twitter oder Google+  - Inhalte, die unterhalten, informieren und engagieren.  Social Media ist ein Ort, wo Gemeinschaften von Menschen mit gemeinsamen Interessen zusammenkommen können."
    },
    {
      id: 4,
      name: "Adnan",
      title: "Software Engineer",
      img: "adnan_cocalic",
      phone: "000 111 222 333",
      email: "adnan.cocalic@mondoit.de",
      skills: [],
      about: "Engagement, Fleiß, Integrität – Worte die Adnan in seinen Aufgaben als Entwickler beschreiben. Adnan würde nie „nein“ zu einer Aufgabe sagen, vermeidet es aber, falsche und unreale Versprechungen bezüglich des Endergebnisses zu geben. Das macht ihn zu einem sehr wertvollen und zuverlässigen Mitglied des Teams."
    },
    {
      id: 5,
      name: "Zoran",
      title: "Software Engineer",
      img: "zoran",
      phone: "000 111 222 333",
      email: "zoran@mondoit.de",
      skills: [],
      about: "Zoran ist Projektmanager und Mitbegründer der MondoIT. Mit seiner strukturiert logischen Art realisiert er alle Wünsche des Kunden. Er ist das Herzstück des Teams und gleichzeitig Senior-Entwickler in den Bereichen PHP, NET, SQL und PhoneGap mit mannigfaltiger Erfahrung in Web, mobilen Anwendungen, Online-Applikations-Entwicklungen und Affiliate-Marketing. Weiterhin erstrecken sich seine Kenntnisse über die Bereiche E-Commerce, Online-Stores und Soziale Netzwerke unter Anwendung von Responsive Websites Designs."
    },
    {
      id: 6,
      name: "Adnan",
      title: "Human Resource Manager",
      img: "adnan_radmilovic",
      phone: "000 111 222 333",
      email: "adnan.radmilovic@mondoit.de",
      about: ""
    },
    {
      id: 7,
      name: "Igor",
      title: "Software Engineer",
      img: "igor",
      phone: "000 111 222 333",
      email: "igor@mondoit.de",
      skills: [],
      about: "Igor ist ein PHP-Experte mit langjähriger professioneller Erfahrung auf diesem Gebiet. Er hat an vielen PHP-Projekten mitgewirkt, die er mit PHP Framework Codelgniter & Slim realisiert hat. Weiterhin besitzt er fundierte Kenntnisse im Arbeiten mit AngularJS und arbeitet sich unglaublich schnell in alle CMS-Systeme ein, die im IT-Markt erscheinen."
    },
    {
      id: 8,
      name: "Marko",
      title: "Software Engineer",
      img: "marko",
      phone: "000 111 222 333",
      email: "marko@mondoit.de",
      skills: [],
      about: "Marko war früher professioneller Wasserball-Spieler. Seine Eigenschaften als Kapitän aus dem Becken hat er in die Organisation und Realisierung von Software-Projekten übertragen. Er beschäftigt sich hauptsächlich mit wissenschaftlicher Forschung und arbeitet zurzeit an der Universität an seiner Doktorarbeit, die das Thema Computerwissenschaften behandelt. Marko ist Senior-Experte in den Bereichen Java, PHP und MySQL."
    },
    {
      id: 9,
      name: "Vildan",
      title: "Senoir Software Engineer",
      img: "vildan_tursic",
      phone: "000 111 222 333",
      email: "vildan.tursic@mondoit.de",
      skills: ["AngularJS", "TypeScript", "NodeJS", "Polymer", "Material Design"],
      about: "Vildan ist JavaScript Experte (Angular-JS, Node-JS and Polymer guru), mit fundiertem Wissen und Verständnis für Web-Technologien und Entwicklungen. Sein Knowhow deckt ebenso die Bereiche der noSQL Datenbanken (Mongodb) sowie Android und Windows Phone Entwicklungen ab. Seine UX und UI Fähigkeiten werden durch seinen künstlerischen Hintergrund hervorgehoben, wodurch er es schaffte maßgeschneiderte Lösungen mit hervorragender Bedienbarkeit zu erschaffen. Mit seiner zielstrebigen Einstellung ist er immer auf der Suche nach den aktuellsten Trends in der Entwicklung."
    },
    {
      id: 10,
      name: "Selvin",
      title: "Senior Software Engineer",
      img: "selvin",
      phone: "000 111 222 333",
      email: "selvin.fehric@mondoit.de",
      about: "Selvin hat einen Master Abschluss in Computerwissenschaften. Er besitzt einen großen Erfahrungsschatz im Software Development. Sein Hauptaugenmerk liegt auf der Entwicklung komplexer Web Applikationen unter der Verwendung von Open-Source-Technologien wie PHP und MySQL auf Linux sowie Microsoft Technologien wie C#, ASP.NET MVC und MSSQL. Er hat weiterhin ein umfassendes Wissen über Web Services, RESTful APIs, WCF Services sowie Erfahrung in der Entwicklung von Desktop Applikationen mittels .NET und Java."
    },
    {
      id: 11,
      name: "Vladimir",
      title: "Software Engineer",
      img: "vladimir",
      phone: "000 111 222 333",
      email: "vladimir@mondoit.de",
      skills: [],
      about: "Vladimir hat mit seiner präzisen Art viele Applikationen im Bereich der mobilen Telefonie realisiert. Als Experte im Organisieren von Database-Architekturen ist er eine große Stütze für das Team. Vladimir ist einer der wenigen IOS-Entwickler, dessen Anwendungen auch heute noch funktionieren."
    },
    {
      id: 12,
      name: "Ognjen",
      title: "Software Engineer",
      img: "ognjen",
      phone: "000 111 222 333",
      email: "ognjen@mondoit.de",
      skills: [],
      about: "Mit mehr als 10 Jahren Arbeitserfahrung auf dem Gebiet Android und ASP.NET ist Ognjen ständig fokussiert auf die neusten Entwicklungen dieser IT-Technologien. Somit kann er unseren Kunden immer die aktuellsten Lösungen aus diesen Bereichen anbieten. Seine Fähigkeit sich  immer genau in den Kunden hineinzuversetzen macht ihn zu einem geschätzten Mitglied unseres Teams."
    },
    {
      id: 13,
      name: "Adnan",
      title: "Software Engineer",
      img: "adnan_memija",
      phone: "000 111 222 333",
      email: "adnan.memija@mondoit.de",
      skills: ["php", "C#", "wordpress"],
      about: "Als Web-Designer, Web-Programmierer und Grafik-Designer hat Adnan Erfahrung bei der Durchführung verschiedener Projekte, in denen er all diese Kenntnisse gleichzeitig einsetzen kann, um ein Ergebnis von höchster Qualität zu erzielen. Die Bandbreite der Technologien mit denen er arbeitet erstreckt sich von Bild- über Videobearbeitungssoftware bis hin zu Web-Anwendungen zur Erstellung von Webseiten. Er kann sich hervorragend auf Kundenbedürfnisse einstellen. Bei ihm wird Customer Experience groß geschrieben."
    }
  ];

  $scope.showMember = function(id) {
    if(id == undefined){
      id = 0;
    }
    $scope.teamMember = $scope.team[id];
  };
  $scope.showMember();

  var idNav = 0;

  $scope.navigate = function (num) {

    idNav += num;

    if (idNav >= $scope.team.length){
      idNav = 0;
    } else if (idNav <= 0) {
      idNav = $scope.team.length;
    }

    $scope.teamMember = $scope.team[idNav];

  }

}]);
