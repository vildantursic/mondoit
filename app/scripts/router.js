var app = angular.module('app');

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$urlMatcherFactoryProvider', function($stateProvider, $urlRouterProvider, $locationProvider, $urlMatcherFactoryProvider) {

  $urlRouterProvider.otherwise("/");
  $urlMatcherFactoryProvider.strictMode(false);

  $stateProvider
    .state('home', {
      url: "/",
      templateUrl: "views/home.html",
      controller: "homeCtrl"
    })
    .state('our-services', {
      url: "/our-services",
      templateUrl: "views/our-services.html",
      controller: "ourServicesCtrl"
    })
    .state('projects', {
      url: "/projects",
      templateUrl: "views/projects.html",
      controller: "projectsCtrl"
    })
    .state('team', {
      url: "/team",
      templateUrl: "views/team.html",
      controller: "teamCtrl"
    })
    .state('contact', {
      url: "/contact",
      templateUrl: "views/contact.html",
      controller: "contactCtrl"
    })
    .state('legal', {
      url: "/legal",
      templateUrl: "views/legal.html"
    })
    .state('termsAndServices', {
      url: "/termsAndServices",
      templateUrl: "views/termsAndServices.html"
    })
    .state('web', {
      url: "/web",
      templateUrl: "views/services/web.html"
    })
    .state('mobile', {
      url: "/mobile",
      templateUrl: "views/services/mobile.html"
    })
    .state('ecommerce', {
      url: "/ecommerce",
      templateUrl: "views/services/ecommerce.html"
    })
    .state('feed', {
      url: "/feed",
      templateUrl: "views/feed.html",
      controller: "feedCtrl"
    });

    $locationProvider.html5Mode({
      enabled: true,
      requireBase: true
    });

}]);
