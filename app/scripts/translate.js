var app = angular.module('app');

app.config(['$translateProvider', function($translateProvider){
  $translateProvider.useSanitizeValueStrategy(null);
  $translateProvider.useStaticFilesLoader({
    prefix: '/translations/',
    suffix: '.json'
  });
  $translateProvider.preferredLanguage('de');

}]);
