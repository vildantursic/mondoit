var gulp = require('gulp'),
    // concat = require('gulp-concat'),
    sync = require('browser-sync'),
    reload = sync.reload,
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    cleanCSS = require('gulp-clean-css'),
    sass = require('gulp-sass');

gulp.task('styles', function () {
  return gulp.src('app/styles/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/styles'))
    .pipe(reload({stream: true}));
});

gulp.task('scripts', function () {
    gulp.src('app/scripts/**/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('dist/scripts'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest("dist/scripts"))
        .pipe(reload({stream: true}));
});

gulp.task('html', function () {
    gulp.src("app/views/**/*.html")
        .pipe(gulp.dest("dist/views"))
        .pipe(reload({stream: true}));
});

gulp.task('translations', function () {
    gulp.src("app/translations/**/*.json")
        .pipe(gulp.dest("dist/translations"))
        .pipe(reload({stream: true}));
});

gulp.task('index', function () {
    gulp.src("app/index.html")
        .pipe(gulp.dest("dist"))
        .pipe(reload({stream: true}));
});

gulp.task('bower', function () {
    gulp.src("app/bower_components/**")
        .pipe(gulp.dest("dist/bower_components"))
        .pipe(reload({stream: true}));
});

gulp.task('browser-sync', function () {
    sync({
        port: 3000,
        ui: {
            port: 8000
        },
        server: {
            baseDir: "./dist"
        }
    })
});

gulp.task('watch', function () {
    gulp.watch("app/styles/**/*.scss", ['styles']);
    gulp.watch("app/scripts/**/*.js", ['scripts']);
    gulp.watch("app/views/**", ['html']);
    gulp.watch("app/bower_components/**", ['bower']);
    gulp.watch("app/index.html", ['index']);
    gulp.watch("app/translations/**/*.json", ['translations']);
});

gulp.task('default', ['styles', 'scripts', 'html', 'index', 'translations', 'bower', 'watch', 'browser-sync']);
